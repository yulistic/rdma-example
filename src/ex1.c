/* Implementation of the examples from "Verbs programming tutorial"
 * by Dotan Barak at OpenSHMEM 2014.
 */
#include <stdio.h>
#include <infiniband/verbs.h>

static int page_size;
static int mode;

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Usage: ./ex1 <mode ex) 0 for send, 1 for recv>\n");
    }
    mode = argv[1];
    printf ("Mode: %d\n", mode);

    // Ex1)
    struct ibv_device **device_list;
    int num_devices;
    int i;

    device_list = ibv_get_device_list(&num_devices);
    if (!device_list) {
        fprintf(stderr, "Error, ibv_get_device_list() failed\n");
        exit(1);
    }

    printf ("Total device count: %d\n", num_devices);

    for (i = 0; i < num_devices; ++i) {
        printf("RDMA device[%d]: name=%s\n", i, ibv_get_device_name(device_list[i]));
        printf("RDMA device[%d]: %s\n", i, ibv_node_type_str());
    }

//    ibv_free_device_list(device_list);


    // Ex2) Open a context and query information.
    // struct ibv_port_attr port_attr;
    // int port_num = 1;

    // Ex3) Open a device allocate a PD and register 3 MRs.
    struct ibv_context *ctx;
    struct ibv_pd *pd;
    struct ibv_mr *mr;

    ctx = ibv_open_device(device_list[0]);  // get context from RDMA device.
    pd = ibv_alloc_pd(ctx); // alloc protection domain.
    if (!pd) {
        fprintf (stderr, "Error, ibv_alloc_pd() failed\n");
        ibv_free_device_list(device_list);
        return -1
    }

    int buf_size = 4096;    // hard-coding.
    page_size = sysconf(_SC_PAGESIZE);
    char *buf = memalign(page_size, buf_size);  // Allocate buffer.
    if (!buf) {
        fprintf(stderr, "Error, cannot allocate buffer.\n");
    }

    ibv_reg_mr(pd, buf, buf_size, IBV_ACCESS_LOCAL_WRITE);
    // ibv_reg_mr(pd, buf, buf_size, IBV_ACCESS_LOCAL_READ);
    // ibv_reg_mr(pd, buf, buf_size, IBV_ACCESS_REMOTE_WRITE);
    if (!mr) {
        fprintf (stderr, "Error, ibv_reg_mr() failed\n");
        return -1;
    }

    // Create Completion Event Channel.
    struct ibv_comp_channel *channel;
    channel = ibv_create_comp_channel(context);
    if(!channel) {
        fprintf(stderr, "Error, ibv_create_come_channel() failed\n");
        return -1;
    }

    // Create Completion Queue.
    struct ibv_cq *cq;
    int cq_size = 100;  // number of Work Completions that CQ can hold.
    cq = ibv_create_cq(ctx, cq_size, NULL, channel, 0);
    //cq = ibv_create_cq(ctx, cq_size, NULL, NULL, 0);
    if (!cq) {
        fprintf(stderr, "Error, ibv_create_cq() failed\n");
        return -1;
    }

    // Create QP (Queue Pair).
    struct ibv_qp *qp;
    struct ibv_qp_init_attr init_attr = {
        .send_cq = cq,
        .recv_cq = cq,
        .cap = {
            .max_send_wr = 1,
            .max_recv_wr = rx_depth,
            .max_send_sge = 1,
            .max_recv_sge = 1
        }
        .qp_type = IBV_QPT_RC
    };

    qp = ibv_create_qp(pd, &init_attr);
    if (!qp) {
        fprintf(stderr, "Error, ibv_create_qp() failed\n");
        return -1;
    }

    if (mode == 0) { // mode == send
        // Post send request.
        struct ibv_sge sg_list = {
            .addr = (uintptr_t) buf,
            .length = buf_size,
            .lkey = mr->lkey
        };
        struct ibv_send_wr wr = {
            .next = NULL,
            .sg_list = &sg_list,
            .num_sge = 1,
            .opcode = IBV_WR_SEND
        };
        struct ibv_send_wr *bad_wr;

        if (ibv_post_send(qp, &wr, &bad_wr)) {
            fprintf(stderr, "Error, ibv_post_send() failed.\n");
            return -1;
        }
    } else if (mode == 1) { // mode == recv
        // Post recv request.
        struct ibv_sge sg_list = {
            .addr = (uintptr_t) buf,
            .length = buf_size,
            .lkey = mr->lkey
        };
        struct ibv_recv_wr wr = {
            .next = NULL,
            .sg_list = &sg_list,
            .num_sge = 1
        }
        struct ibv_recv_wr *bad_wr;

        if (ibv_post_recv(qp, &wr, &bad_wr)) {
            fprintf(stderr, "Error, ibv_post_recv() failed\n");
            return -1;
        }
    } else {
        fprintf (stderr, "Error, not supported mode: %d\n", mode);
        goto destroy;
    }


    // From Polling......


destroy:
    // Destroy.
    if (ibv_destroy_qp(qp)) {
        fprintf(stderr, "Error, ibv_destroy_qp() failed \n");
        return -1;
    }

    if (ibv_destroy_cq(cq)) {
        fprintf(stderr, "Error, ibv_destroy_cq() failed\n");
        return -1;
    }

    if (ibv_destroy_comp_channel(channel)) {
        fprintf (stderr, "Error, ibv_destroty_comp_channel() failed\n");
        return -1;
    }

    if (ibv_dereg_mr(mr)) {
        fprintf (stderr, "Error, ibf_dereg_mr() failed\n");
        return -1;
    }

    if (ibv_dealloc_pd(pd)) {    // dealloc protection domain.
        fprintf (stderr, "Error, ibv_dalloc_pd() failed\n");
        return -1;
    }

    if (ibv_close_device(ctx)) {    // close context.
        fprintf (stderr, "Error, ibv_close_device() failed\n");
        return -1;
    }

    if (ibv_free_device_list(device_list)) {   // free device list.
        fprintf (stderr, "Error, ibv_free_device_list() failed\n");
        return -1;
    }
}

