CC=gcc
FLAGS= -g -Og
LIBS= -libverbs


bin/ex1: src/ex1.c
	astyle --recursive "./src/*.c" --style=linux
	$(CC) -o $@ $^ $(LIBS)
